var requirejs = require('requirejs');

requirejs.config({
	paths: {
		'app': './app',
		'mysql_legacy':  './mysql_legacy',
		'utils': './utils.js',
		'client_config': './routes/api/client_config',
		'logout': './routes/api/logout',
		'mcc_auth': './routes/api/mcc_auth',
		'static_ip_range': './routes/api/static_ip_range'
	},
	shim: {
	},
	nodeRequire: 'require'
});

var initDependency = function(commonDependency) {
    var extraDependency = [
      'mysql_legacy',
      'utils',
      'client_config',
      'logout',
      'mcc_auth',
      'static_ip_range'
    ];

    return commonDependency.concat(extraDependency).concat(bizDependency);
};

requirejs(initDependency(['app', 'https', 'fs'
    ]),
    function (app, https, fs) {
        'use strict';
      var options = {
				key: fs.readFileSync(__dirname + '/certs/privatekey.pem'),
				cert: fs.readFileSync(__dirname + '/certs/certificate.pem')
			};

			https.createServer(options, app).listen(app.get('port'), function(){
			  console.log('Express server listening on port ' + app.get('port'));
			});  
    }
);
