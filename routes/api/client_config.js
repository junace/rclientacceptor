/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 8. 28.
 * Time: 오후 1:40
 * To change this template use File | Settings | File Templates.
 */

var mysql_legacy = require('../../mysql_legacy');
var moment = require('moment');
var http = require('https');
var async = require('async');
var BufferedReader = require('buffered-reader');
var DataReader = BufferedReader.DataReader;
var mcc_auth = require('./mcc_auth');
var mysql = require('mysql');
var i18n = require('i18n');

// setup global database connection
var connection = mysql_legacy.getConnection([{
	host : 'localhost',
	user : 'root' ,
	password : 'hsmsecure',
	database : 'ssl_vpn_full'
}]);

/*var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root' ,
	password : 'hsmsecure',
	database : 'ssl_vpn_full'
});*/

exports.get_config = function(req, res) {
	var userId = req.param('userId');
	var password = req.param('password');
    // mode : online, offline
	var mode = req.param('mode');
	var clientIp = req.connection.remoteAddress;

	console.log('remoteAddress => ' + clientIp);
	var ctx = {
		userId:userId,
		password:password,
		mode:mode
	};
	async.series([
		function(callback) {
            // 1. 그룹 명 저장
			var query_string = 'select W_GROUP from WEBDBINFO where W_ID=1';
			connection.query(query_string, function(err, results)
			{
				if(err)
				{
					callback(err);
				}
				else
				{
					if(!results || results.length < 1)
					{
						callback(new Error(res.__('WEBDBINFO 에러')));
					}
					else
					{
						ctx.groupName = results[0].W_GROUP;
						callback(null);
					}
				}
			});
		},
		function(callback) {
            // 2. 접속 제한 체크
			var query_string = 'select * from USERINFO_OUT where U_EMAIL=?';
			connection.query(query_string,[userId], function(err, results) {
				if(results.length == 1 && results[0].U_REJECT != 0)
				{
					var error = new Error();
					error.message = res.__('접속제한=현재 사용자는 접속할 수 없습니다');
					callback(error);
				}
				else
				{
					callback(null);
				}
			});
		},
		function(callback) {
            // 3. online , offline 에 따라 분기
			var auth_cb = function(err, jsonResult) {
				ctx.mcc_auth_json_result = jsonResult;
				if(err) {
					console.log('mcc auth failed');
					mysql_legacy.legacy_log(userId, clientIp, 10, res.__(ctx.mcc_auth_json_result.message))
				}

				callback(err);
			};
			if(mode == 'offline')
			{
				mcc_auth.auth_offline(userId, password, auth_cb);
			}
			else {
				mcc_auth.auth_online(userId, password, auth_cb);
			}
		},
		function(callback) {
            // 4. mcc 인증 성공
			console.log('server ip decision');
			var query_string = 'select * from SYSTEMCONF where S_ID=1';
			connection.query(query_string, function(err, results) {
				console.log('1');
			//	console.log(results[0]);
				if(results[0].S_PROTOCOL == 0)
				{
					ctx.protocol = 'tcp';
				}
				else
				{
					ctx.protocol = 'udp';
				}
                ctx.network_prefix = results[0].S_NETWORK;

				ctx.cipher = results[0].S_CIPHER;
				ctx.compress = (results[0].S_USERNAT == 1) ? true : false; // why S_USERNAT?
                // 서버 ip 리스트 생성 : ctx.remoteArray
				//TODO: server ip 분배.... 로직이 이상하지만, 기존 소스 그대로 따랐음.. 필히 리팩토링 필요.
				ctx.remoteArray = [];
				if(results[0].S_HA && results[0].S_HA.length != 0 && results[0].S_LBIP1 && results[0].S_LBIP1.length != 0 && results[0].S_LBIP2 && results[0].S_LBIP2.length != 0  )
				{
					var r = Math.floor((Math.random()*2)) % 2;
					if(r == 0)
					{
						ctx.remoteArray.push(results[0].S_LBIP1 + ' ' + results[0].S_PORT);
						ctx.remoteArray.push(results[0].S_LBIP2 + ' ' + results[0].S_PORT);
					}
					else
					{
						ctx.remoteArray.push(results[0].S_LBIP2 + ' ' + results[0].S_PORT);
						ctx.remoteArray.push(results[0].S_LBIP1 + ' ' + results[0].S_PORT);
					}
				}
				else {
					var natString = results[0].S_NAT;
					if(natString && natString.length != 0)
					{
						var natArr = natString.split('|');
						natArr.forEach(function(e) {
							ctx.remoteArray.push(e + ' ' + results[0].S_PORT);
						});
						callback(null);
					}
					else
					{
						if(results[0].S_DUAL_FLAG == 2)
						{
							if(results[0].S_DUAL_MODE == 0)
							{
								ctx.remoteArray.push(results[0].S_IP + ' ' + results[0].S_PORT);
								ctx.remoteArray.push(results[0].S_DUAL_IP + ' ' + results[0].S_PORT);
							}
							else
							{
								var currentActive;
								var currentFlag = 0;
								async.series(
									[ function(callback) {
											new DataReader('/proc/net/bonding/bond0', {encoding:'utf8'})
											.on ("error", function (error){
												callback(null);
											})
											.on ("line", function (line){
												var idx = line.indexOf('Currently Active Slave:');
												if(idx != -1)
												{
													currentActive = line.split('Current Active Slave:')[1].trim();
												}
											})
											.on ("end", function (){
												callback(null);
											})
											.read ();
									},
									function(callback) {
										var dualActiveArray = results[0].S_DUAL_ACTIVE_SELECT.split('-');
										var dualActive1 = dualActiveArray[0].split(':');
										var dualActive2 = dualActiveArray[1].split(':');
										if(currentActive == dualActive1[0])
										{
											currentFlag = 0;
										}
										else if(currentActive == dualActive1[1])
										{
											currentFlag = 1;
										}
										callback(null);
									}],
								function(err, results) {
									console.log('async series');
									if(currentFlag == 0)
									{
										ctx.remoteArray.push(results[0].S_IP + ' ' + results[0].S_PORT);
										ctx.remoteArray.push(results[0].S_DUAL_IP + ' ' + results[0].S_PORT);
									}
									else
									{
										ctx.remoteArray.push(results[0].S_DUAL_IP + ' ' + results[0].S_PORT);
										ctx.remoteArray.push(results[0].S_IP + ' ' + results[0].S_PORT);
									}
									callback(null);
								});
							}
						}
						else
						{
							ctx.remoteArray.push(results[0].S_IP + ' ' + results[0].S_PORT);
							callback(null);
						}
					}
				}
			});
		},
		function(callback) { // host file
            // 5. host file 생성
			console.log('host file');

			var queryString = 'select * from GROUPLIST where G_NAME=?';
			connection.query(queryString, [ctx.groupName], function(err, results) {
		//		console.log(results);
				if(err) {
					console.log(err.localizedMessage);
					callback(err);
					return;
				}
				if(!(results && results.length > 0))
				{
					console.log('GROUPLIST table is empty');
					callback(new Error(res.__('일치하는 그룹명이 존재하지 않습니다')));
					return;
				}
				ctx.groupId = results[0].G_ID;
				ctx.cache = results[0].G_CACHE;
				ctx.idle = results[0].G_IDLE;
				ctx.activexdelete = results[0].G_ACTIVEX;

				if(results[0].G_TDIFORT)
				{
					var na = results[0].G_TDIFORT.split('|');
					ctx.groupIpCClassBegin = na[0];
					ctx.groupIpCClassEnd = na[1];
				}
				else
				{
					ctx.groupIpCClassBegin = 0;
					ctx.groupIpCClassEnd = 0;
				}
				console.log('ip class begin => ' + ctx.groupIpCClassBegin + ' end => ' + ctx.groupIpCClassEnd);
				if( results[0].G_HOST && results[0].G_HOST.length != 0)
				{
                    // TODO : 사용 유무 확인 필요
					ctx.hostMapping = [];
					var hostArray = results[0].G_HOST.split('|');
					async.eachSeries(hostArray,
						function(item, callback) {
							var queryString = 'select S_IP,S_INTIP from SERVERLIST where S_ID=?';
							connection.query(queryString, [item], function(err, results) {
								ctx.hostMapping.push(
									{
										ip:results[0].S_IP,
										mapping:results[0].S_INTIP
									});
								callback(null);
							});
						},
						function(err)
						{
							callback(null);
						});

				}
				else
				{
					callback(null);
				}
			});
		},
		function(callback) {
            // 6. client 가상 IP 결정
			console.log('vip');
			console.log(ctx.mcc_auth_json_result);
			console.log(ctx.mcc_auth_json_result.vip);
            // static ip
			if(ctx.mcc_auth_json_result.vip && ctx.mcc_auth_json_result.vip.length != 0 && ctx.mcc_auth_json_result.vip[0] && ctx.mcc_auth_json_result.vip[0].length > 0)
			{
        ctx.ipAssignMode = 'static';
				var vipArr = ctx.mcc_auth_json_result.vip[0].split('.');
				console.log('vipArr => ');
				console.log(vipArr);
				var queryString = "select I_CERTNO,I_FLAG, I_CCLASS, I_DCLASS from IPTABLE where I_CCLASS=? and I_DCLASS=?";
				connection.query(queryString, [vipArr[2], vipArr[3]], function(err, results) {
					if(err) {
						console.log(err.message);
						callback(err);
					}
					else
					{
						if(results.length != 1)
						{
							console.log('inproper ip');
							callback(new Error('inpropoer ip'));
							return;
						}
						console.log(results[0]);
						ctx.certNo = results[0].I_CERTNO;
						ctx.ip2 = results[0].I_CCLASS;
						ctx.ip3 = results[0].I_DCLASS;
						console.log('static ip => ' + '172.16.' + ctx.ip2 + '.' + ctx.ip3);
						if(results[0].I_FLAG != '0') // 이미 IP 사용중... 커넥션 끊어준다.
						{
							var net = require('net');
							var client = net.connect(7505, 'localhost', function () {
								client.end('kill client' + results[0].I_CERTNO + '\n');
								client.on('error', function (err) {
									console.log('error:', err.message);
								});

								client.on('close', function () {
									callback(null);
									console.log('Connection closed');
								});
							});
						}
						else
						{
							var queryString = 'update IPTABLE set I_FLAG="1" where I_CERTNO=?';
							connection.query(queryString, [results[0].I_CERTNO], function(err, results) {
								callback(err);
							});
						}
					}
				});
			}
			else
			{
        ctx.ipAssignMode = 'dynamic';
                // dynamic address
				var query_string = 'select I_CERTNO,I_CCLASS,I_DCLASS from IPTABLE where I_FLAG="0" and IS_STATIC=0 and I_CCLASS >= ? and I_CCLASS <= ? order by I_CCLASS,I_DCLASS asc limit 0,1';
				connection.query(query_string, [ctx.groupIpCClassBegin, ctx.groupIpCClassEnd], function(err, results) {
					if(err) {
						console.log('IPTABLE access failed');
						console.log(err.message);
						callback(err);
						return;
					}
					else
					{
						if(results.length != 1)
						{
							console.log('inproper ip');
							callback(new Error('inpropoer ip'));
							return;
						}
					}
					ctx.certNo = results[0].I_CERTNO;
					ctx.ip2 = results[0].I_CCLASS;
					ctx.ip3 = results[0].I_DCLASS;
					//console.log('dynamic ip => ' + '172.16.' + ctx.ip2 + '.' + ctx.ip3);
					var queryString = 'update IPTABLE set I_FLAG="1" where I_CERTNO=?';
					connection.query(queryString, [results[0].I_CERTNO], function(err, results) {
						callback(err);
					});
				});
			}
		},
		function(callback) {
            // 7.
			console.log('client conf');
			ctx.clientCert = '';
			ctx.clientKey = '';
			ctx.caCert = '';
			var query_string = 'select * from USERINFO_OUT where U_EMAIL=?';
			connection.query(query_string,[userId], function(err, results) {
				async.parallel([
					function(callback) {
                        // mmc 인증 사용 자 정보 업데이트
						if(results.length == 1) // 기존 사용자 존재
            {
              if (results[0].U_IP != (ctx.ip2 + '.' + ctx.ip3)) // 기존 ip free
              {
                var queryString = 'update IPTABLE set I_FLAG="0" where I_CERTNO=?';
                console.log('free previous ip , certNo => ' + results[0].U_CERTNO);
                connection.query(queryString, [results[0].U_CERTNO], function (err, results) {
                  console.log('free previous ip  success');
                });
              }
            }

            var query_string = "replace into USERINFO_OUT(U_EMAIL,U_CERTNO,U_IP,U_GROUP,U_JUMINNO2,U_MAC,U_USEREMAIL,U_LOGINFAIL,U_DATE,U_NAME) values(?,?,?,?,'','','','0',now(),'')"
            connection.query(query_string, [userId, ctx.certNo, ctx.ip2 + '.' + ctx.ip3, ctx.groupId], function(err, results) {
              callback(err);
            });
					},
					function(callback) {
                        // mcc 인증 사용 자 인증서 처리
						new DataReader('/securegate/sslvpn/full/config/certs/client' + ctx.certNo + '.crt', {encoding:'utf8'})
						.on ("error", function (error){
                            // TODO : 인증서 로드 에러 시 처리 필요
							callback(null);
						})
						.on ("line", function (line){
							ctx.clientCert += line + '\n';
						})
						.on ("end", function (){
							callback(null);
						})
						.read ();
					},
					function(callback) {
                        // mcc 인증 사용 자 개인키 처리
						new DataReader('/securegate/sslvpn/full/config/certs/client' + ctx.certNo + '.key', {encoding:'utf8'})
							.on ("error", function (error){
							callback(null);
						})
							.on ("line", function (line){
							ctx.clientKey += line + '\n';
						})
							.on ("end", function (){
							callback(null);
						})
						.read ();
					},
					function(callback) {
                        // mcc 인증 사용 자 ca 인증서 처리
						new DataReader('/securegate/sslvpn/full/config/certs/ca.crt' , {encoding:'utf8'})
							.on ("error", function (error){
							callback(null);
						})
							.on ("line", function (line){
							ctx.caCert += line + '\n';
						})
							.on ("end", function (){
							callback(null);
						})
						.read ();
					}
				],
				function(err, results) {
					callback(null);
				});
			});
		}
	],
	function(error, results) {
        // *. 결과 처리, 에러 처리
		console.log('final result');
		if(error != null)
		{
			if(ctx.mcc_auth_json_result)
			{
				console.log(ctx.mcc_auth_json_result);
				if(error.message != null)
				{
					mysql_legacy.legacy_log(userId, clientIp, 10, error.message);
				}
				else
				{
					mysql_legacy.legacy_log(userId, clientIp, 10, ctx.mcc_auth_json_result.Message[0]);
				}

				res.send(
					{
						success: false,
						errorCode: ctx.mcc_auth_json_result.Error[0],
						errorMessage:ctx.mcc_auth_json_result.Message[0]
					});
			}
			else
			{
				console.log('error =>' + error.message);
				mysql_legacy.legacy_log(userId, clientIp, 10, error.message);
				res.send(
					{
						success:false,
						errorCode: -1,
						errorMessage: error.message,
					});
			}
            // 사용자 접속 정보 업데이트
			query_string = "update USERINFO_OUT set U_LOGINFAIL='0',U_RIP=?,U_SENT='',U_RECEIVED=adddate(now(),-1),U_LASTLOGIN=now(),U_STATE='0',U_VALUE=? where U_EMAIL=?";
			connection.query(query_string, [clientIp, 'PC', userId], function(err, results) {
				if(err) {
					console.log(err.message);
				}
			});
			console.log('error occurred ' + error.message);
		}
		else
		{
			console.log('success');
			console.log(clientIp);

			mysql_legacy.login_out_log(userId, clientIp, ctx.network_prefix + '.' + ctx.ip2 + '.' + ctx.ip3, true);

      var is_static_ip = false;
      console.log(ctx.mode);
      if(ctx.mode !== 'offline') {
        var isStaticIp;
        if(ctx.ipAssignMode == 'static') {
          isStaticIp = true;
        }
        else {
          isStaticIp = false;
        }
        var query_string = 'insert into MCC_AUTH_TOKEN_CACHE(LOGIN_ID, AUTH_TOKEN, ASSIGNED_IP, IS_STATIC_IP) values(?, ?, ?, ?) ' +
          'on duplicate key update AUTH_TOKEN=values(AUTH_TOKEN), ASSIGNED_IP=values(ASSIGNED_IP) , IS_STATIC_IP=values(IS_STATIC_IP)';
        console.log('ip => ' + ctx.network_prefix + '.' + ctx.ip2 + '.' + ctx.ip3 + ' ' + (isStaticIp?'static':'dynamic'));
        connection.query(query_string, [userId, password, ctx.network_prefix + '.' + ctx.ip2 + '.' + ctx.ip3, isStaticIp], function(err, results) {
          console.log('auth token cache insert , error => ' + err);
          //정적 할당시, 기존에 해당 IP를 정적으로 할당 받은 사용자가 있으면 정적 flag삭제.
          query_string = "update MCC_AUTH_TOKEN_CACHE set IS_STATIC_IP=false where ASSIGNED_IP=?";
          connection.query(query_string, [ctx.network_prefix + '.' + ctx.ip2 + '.' + ctx.ip3], function(err, results) {
            if(err) {
              console.log(err.message);
            }
            else {
              console.log('previous static flag cleared');
              console.log(results);
            }
          });
        });
      }

      var query_string = "update USERINFO_OUT set U_LOGINFAIL='0',U_RIP=?,U_GROUP=?, U_SENT='',U_RECEIVED=adddate(now(),-1),U_LASTLOGIN=now(),U_STATE='0',U_VALUE=? where U_EMAIL=?";
      connection.query(query_string, [clientIp, ctx.groupId, 'PC', userId], function(err, results) {
        if(err) {
          console.log(err.message);
        }
      });



			ctx.redirection = 'N';
			ctx.reconnect = 1;
		//	ctx.connect_message = '보안접속중입니다... 잠시만 기다려주세요'; //TODO: localize 필요.

			ctx.connect_message = res.__('보안접속중입니다... 잠시만 기다려주세요');
		/*
			if(req.headers['accept-language'] == 'ko')
			{
				ctx.connect_message = '보안접속중입니다... 잠시만 기다려주세요'
			}
			else if(req.headers['accept-language'] == 'ja')
			{
				ctx.connect_message = "セキュリティ接続中です。しばらくお待ちください";
			}
			else
			{
				ctx.connect_message = '보안접속중입니다... 잠시만 기다려주세요'
			}
*/
			//console.log(req.headers);
			console.log(ctx.connect_message);

			var ret = {
				success:true,
				protocol: ctx.protocol,
				remoteArray:ctx.remoteArray,
				cipher:ctx.cipher,
				logLevel:3,
				compress:ctx.compress,
				rootCert:ctx.caCert,
				clientCert:ctx.clientCert,
				clientKey:ctx.clientKey,
				userId: userId,
				redirection: ctx.redirection,
				reconnect: ctx.reconnect,
				connect_message: ctx.connect_message,
				cache:ctx.cache,
				idle:ctx.idle,
				activexdelete: ctx.activexdelete
			};
			//console.log(ret);
			res.send(
				ret
			);
		}
	});

}
