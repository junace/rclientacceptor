/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 9. 10.
 * Time: 오후 5:04
 * To change this template use File | Settings | File Templates.
 */
var mysql_legacy = require('../../mysql_legacy');
var moment = require('moment');
var xml2js = require('xml2js');
var http = require('https');
var async = require('async');
var BufferedReader = require('buffered-reader');
var DataReader = BufferedReader.DataReader;
var mysql = require('mysql');

/*var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root' ,
	password : 'hsmsecure',
	database : 'ssl_vpn_full'
});*/

// setup global database connection
var connection = mysql_legacy.getConnection([{
	host : 'localhost',
	user : 'root' ,
	password : 'hsmsecure',
	database : 'ssl_vpn_full'
}]);

var authTokenCacheArray = [];
var cache_checked = false;
var sysconf_checked = false;

function check_sysconf(cb) {
	if(sysconf_checked)
	{
		return;
	}
	sysconf_checked = true;
	var query_string = 'SHOW COLUMNS FROM SYSTEMCONF LIKE "S_SENSITIVITY"';
	connection.query(query_string, function(err, results) {
		console.log(results);
		if(results.length < 1)
		{
			// create table
			query_string = 'ALTER TABLE SYSTEMCONF ADD S_SENSITIVITY VARCHAR(3)';

			connection.query(query_string, function(err, results) {
				if(err) {
					console.log('SYSTEMCONF S_SENSITIVITY field add failed');
					console.log(err.errorMessage);
				}
				else {
					console.log('SYSTEMCONF S_SENSITIVITY field added');
				}
				cb();
			});
		}
	});
}

function check_mcc_auth_token_cache(cb) {
	var query_string = 'SHOW TABLES LIKE "MCC_AUTH_TOKEN_CACHE"';
	connection.query(query_string, function(err, results) {
		console.log(results);
		if(results.length < 1)
		{
			// create table
			query_string = 'CREATE TABLE MCC_AUTH_TOKEN_CACHE(' +
				'TOKEN_ID INT NOT NULL AUTO_INCREMENT, ' +
				'LOGIN_ID VARCHAR(64) NOT NULL UNIQUE, ' +
				'AUTH_TOKEN VARCHAR(64) NOT NULL, ' +
				'ASSIGNED_IP VARCHAR(16) NOT NULL, ' +
        'IS_STATIC_IP TINYINT DEFAULT 0' +
				'CREATE_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
				'PRIMARY KEY (TOKEN_ID) ) ';
			connection.query(query_string, function(err, results) {
				if(err) {
					console.log('MCC_AUTH_TOKEN_CACHE table create failed');
					console.log(err.errorMessage);
				}
				else {
					console.log('MCC_AUTH_TOKEN_CACHE table created');
				}
				cb(err);
			});
		}
		else
		{
      console.log('checking MCC_AUTH_TOKEN_CACHE TABLE');
      var query_string = 'SHOW COLUMNS FROM MCC_AUTH_TOKEN_CACHE LIKE "IS_STATIC_IP"';
      connection.query(query_string, function(err, results) {
        console.log(results);
        if(results.length < 1)
        {
          query_string = 'ALTER TABLE MCC_AUTH_TOKEN_CACHE ADD IS_STATIC_IP TINYINT DEFAULT 0';

          connection.query(query_string, function(err, results) {
            if(err) {
              console.log('MCC_AUTH_TOKEN_CACHE IS_STATIC_IP field add failed');
              console.log(err.errorMessage);
            }
            else {
              console.log('MCC_AUTH_TOKEN_CACHE IS_STATIC_IP field added');
            }
            cb(err);
          });
        }
      });

			query_string = 'select * from MCC_AUTH_TOKEN_CACHE';
			connection.query(query_string, function(err, results) {
				results.forEach(function(ele) {
					authTokenCacheArray.push({loginId : ele.LOGIN_ID, authToken : ele.AUTH_TOKEN});
				});
				cb();
			});
		}
	});
}

//check_mcc_auth_token_cache();

exports.auth_online = function(loginId, authToken , cb) {
	var ctx = {};
	var jsonResult = {
		mode : 'online',
		AuthVpnUserResult: {
			Error:[0],
			Message:[''],
			vip:['']
		}
	};
	async.series([
		function(callback) {
            // 1. mcc_auth_token_cache table 확인 후 생성
			if(cache_checked == false) {
				check_mcc_auth_token_cache(function() {
					cache_checked = true;
					callback(null);
				});
			}
			else
			{
				callback(null);
			}
		},
		function(callback) {
            // 2. ws 서버 정보 추출
			var query_string = 'select * from WEBDBINFO where W_ID=1';
			connection.query(query_string, function(err, results) {
				if(!results || results.length < 1)
				{
					if(err) {
						console.log(err.message);
					}
					console.log(results);
					callback(new Error());
					return;
				}
				var serverIp = results[0].W_IP;
				var	serverPort = results[0].W_PORT;
				var serverUri = results[0].W_URI;

				console.log('serverIp =>' + serverIp + ' serverPort : ' + serverPort + ' serverUri : ' + serverUri);


				ctx.mcc_auth_request_options = {
					hostname: serverIp,
					port: serverPort,
					path: serverUri,
					method: 'POST',
					rejectUnauthorized:false,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Cache-Control': 'no-cache',
						'Host' : serverIp + ':' + serverPort
					}
				};

				ctx.loginIdFieldName = results[0].W_LOGINID;
				ctx.tokenFieldName = results[0].W_TOKEN;
				callback(null);
			});
		},
		function(callback) {
            // 3.
			var query_string = 'select * from CLIENT_CONFIG where C_URI="index.jsp"';
			connection.query(query_string, function(err, results) {
				var retryLimit;
				var reconnect;
				ctx.retryLimit = results[0].C_FAIL;
				ctx.reconnect = results[0].C_OUTUSER;
				callback(null);
			});
		},
		function(callback) {
            // 4. mcc 인증 요청
			var request = http.request(ctx.mcc_auth_request_options, function(response) {
				ctx.ws_response = response;
				callback(null);
			});
			request.on('error', function(e) {

				console.log('authentication to mcc failed');
				console.log(e.message);
				callback(new Error());
			});
			request.write(ctx.loginIdFieldName + '=' + loginId + '&' + ctx.tokenFieldName + '=' + authToken);
			console.log(ctx.loginIdFieldName + '=' + loginId + '&' + ctx.tokenFieldName + '=' + authToken);
			request.end();
		},
		function(callback) {
            // 5. mcc 인증 결과 처리 callback 등록
			console.log('STATUS: ' + ctx.ws_response.statusCode);
			console.log('HEADERS: ' + JSON.stringify(ctx.ws_response.headers));
			ctx.ws_response.setEncoding('utf8');
			ctx.ws_response.on('data', function (chunk) {
						console.log('BODY: ' + chunk);
				ctx.jsonBody = chunk;
				callback(null);
			});
		},
		function(callback) {
            // 6. mcc 인증 결과 처리
			console.log('before parse');
			var parser = new xml2js.Parser();
			parser.parseString(ctx.jsonBody, function (err, result) {
				jsonResult.mcc_auth_json_result = result.AuthVpnUserResult;
				console.log(result);
				if(result.AuthVpnUserResult.Error[0] != '0')
				{
					callback(new Error());
				}
				else
				{
					callback(null);
				}
			});
		}],
	function(err, results) {
        // *. 결과 리턴, 에러 처리
		cb(err, jsonResult.mcc_auth_json_result);
	});
};


exports.auth_offline = function(loginId, authToken, cb) {
	var jsonResult = {
		mode : 'offline',

		AuthVpnUserResult: {
			Error:[0],
			Message:[''],
			vip:['']
		}
	};

	var query_string = 'select AUTH_TOKEN, ASSIGNED_IP from MCC_AUTH_TOKEN_CACHE where LOGIN_ID=?';
	connection.query(query_string, [loginId], function(err, results) {
		if(err) {
			console.log('MCC_AUTH_TOKEN_CACHE table access failed');
			jsonResult.AuthVpnUserResult.Error[0] = -1;
			jsonResult.AuthVpnUserResult.Message[0] = 'MCC_AUTH_TOKEN_CACHE table access failed';
			cb(err, jsonResult.AuthVpnUserResult);
		}
		else
		{
			if(results.length != 1)
			{
				console.log('previous auth record is not exist');
				jsonResult.AuthVpnUserResult.Error[0] = -1;
				jsonResult.AuthVpnUserResult.Message[0] = 'previous auth record is not exist';
				cb(null, jsonResult.AuthVpnUserResult);
			}
			else {
				if(authToken == results[0].AUTH_TOKEN) {
					console.log('offline authentication success');
					jsonResult.AuthVpnUserResult.Error[0] = 0;
          if(results[0].IS_STATIC_IP) {
            jsonResult.AuthVpnUserResult.vip[0] = results[0].ASSIGNED_IP;
          }
          cb(null, jsonResult.AuthVpnUserResult);
				}
				else
				{
					console.log('offline auth_token mismatch');
          console.log('saved auth token : ' + results[0].AUTH_TOKEN);
          console.log('submitted auth token : ' + authToken);
					jsonResult.AuthVpnUserResult.Error[0] = -1;
					jsonResult.AuthVpnUserResult.Message[0] = 'offline auth_token mismatch';
					cb(new Error(), jsonResult.AuthVpnUserResult);
				}
			}
		}
	});
};