/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 11. 14.
 * Time: 오후 2:28
 * To change this template use File | Settings | File Templates.
 */
var mysql_legacy = require('../../mysql_legacy');

exports.client_logout = function(req, res) {
	var userId = req.param('userId');

	var clientIp = req.connection.remoteAddress;
	mysql_legacy.login_out_log(userId, clientIp, '', false);
	res.send({result:'Ok'});
};