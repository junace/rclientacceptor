/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 9. 9.
 * Time: 오후 1:08
 * To change this template use File | Settings | File Templates.
 */
var mysql = require('mysql');
var moment = require('moment');
var async = require('async');

// setup global database connection
var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root' ,
	password : 'hsmsecure',
	database : 'ssl_vpn_full'
});

var mcc_static_ip_ranges = [];

function check_ip_ranges_table() {
	var query_string = 'SHOW TABLES LIKE "MCC_STATIC_IP_RANGES"';
	connection.query(query_string, function(err, results) {
		if(results.length < 1)
		{
			// create table
			query_string = 'CREATE TABLE MCC_STATIC_IP_RANGES(' +
				'RANGE_ID INT NOT NULL AUTO_INCREMENT, ' +
				'START_IP VARCHAR(16) NOT NULL, ' +
				'END_IP VARCHAR(16) NOT NULL, ' +
				'CREATE_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
				'PRIMARY KEY (RANGE_ID) ) ';
			connection.query(query_string, function(err, results) {
				if(err) {
					console.log('MCC_STATIC_IP_RANGES table create failed');
					console.log(err.errorMessage);
				}
				else {
					console.log('MCC_STATIC_IP_RANGES table created');
				}
			});
			query_string = 'ALTER TABLE IPTABLE ADD IS_STATIC tinyint DEFAULT 0';
			connection.query(query_string, function(err, results) {
				if(err) {
					console.log('IPTABLE table add column failed');
					console.log(err.errorMessage);
				}
				else {
					console.log('IPTABLE IS_STATIC column added');
				}
			});
		}
		else
		{
			query_string = 'select * from MCC_STATIC_IP_RANGES';
			connection.query(query_string, function(err, results) {
				results.forEach(function(ele) {
					mcc_static_ip_ranges.push({startIp : ele.START_IP, endIp : ele.END_IP});
				});
			});
		}
	});
}

check_ip_ranges_table();

exports.update_mcc_ip_ranges = function(req, res)
{
	console.log('update_mcc_ip_range');
	var rangesString = req.param('ip_ranges');
	if(!rangesString || rangesString.length == 0)
	{
		res.send(500, 'bad ip range format');
		return;
	}
	async.series(
		[
			function(callback) {
                // 변경 적용 시 기존 데이터 리셋 후 작업
				var query_string = 'update IPTABLE SET IS_STATIC=0';
				connection.query(query_string, function(err, results) {
					if(err) {
						console.log('IPTABLE IS_STATIC column update false');
						console.log(err.message);
					}
					else
					{
						console.log('IPTABLE IS_STATIC column update success');
					}
					callback(err);
				});
			},
			function(callback) {
				var query_string = 'TRUNCATE TABLE MCC_STATIC_IP_RANGES';
				connection.query(query_string, function(err, results) {
					console.log('old MCC_STATIC_IP_RANGES table truncated');
                    // 192.168.1.1~192.168.1.128,192.168.2.1~192.168.2.64
					rangeArray = rangesString.split(',');
					query_string = 'INSERT INTO MCC_STATIC_IP_RANGES (START_IP, END_IP) VALUES (? , ?)';
					mcc_static_ip_ranges = [];
					async.eachSeries(rangeArray,
						function(item, callback) {
							var range = item.split('~');
							mcc_static_ip_ranges.push({startIp : range[0], endIp : range[1]});
							connection.query(query_string, [range[0], range[1]], function(err, results) {
								var ipPartArray1 = range[0].split('.');
								var ipPartArray2 = range[1].split('.');
								var query_string;
                                // mcc_static_ip_ranges 등록 후 iptable 에 static 체크
								if(ipPartArray1[2] == ipPartArray2[2]) {
									query_string = 'update IPTABLE SET IS_STATIC=1 where I_CCLASS = ' + ipPartArray1[2] + ' AND '
										+ 'I_DCLASS >= ' + ipPartArray1[3] + ' AND I_DCLASS <= ' + ipPartArray2[3];
								}
								else
								{
									query_string = 'update IPTABLE SET IS_STATIC=1 where (I_CCLASS > ' + ipPartArray1[2] + ' AND '
										+ 'I_CCLASS < ' + ipPartArray2[2] + ') OR ( '+ 'I_CCLASS = ' + ipPartArray1[2] + ' AND I_DCLASS >= '
										+ ipPartArray1[3] + ') OR ( I_CCLASS = ' + ipPartArray2[2] + ' AND I_DCLASS <= '
										+ ipPartArray2[3] + ')';
								}
								connection.query(query_string, function(err, results) {
									console.log('startIp => ' + range[0] + ' endIp => ' + range[1]);
									if(err) {
										console.log('IPTABLE IS_STATIC column update false');
										console.log(err.message);
									}
									else
									{
										console.log('IPTABLE IS_STATIC column update success');
									}
									callback(err);
								});
							});
						},
						function(err) {
							callback(err);
						});
				});
			}],
		function(err, results) {
			if(err) {
				res.send(500, 'error occurred');
			}
			else
			{
				res.send(200, 'updated');
			}
		});
};

exports.get_free_ip_for_dhcp = function() {
	var compareString = '';
	mcc_static_ip_ranges.forEach(function(ele) {

	});
	var query_string = 'select I_CERTNO,I_CCLASS,I_DCLASS from IPTABLE where I_GROUPID=1 and I_FLAG="0" and IS_STATIC=0 order by I_CCLASS,I_DCLASS asc limit 0,1';
};