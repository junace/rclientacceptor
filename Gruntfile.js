module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: ['*.js', 'routes/**/*.js', '!Gruntfile.js'],
        // the location of the resulting JS file
        dest: 'dist/<%= pkg.name %>.concat.js'
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: "./",
          //out: 'dist/<%= pkg.name %>.js',
          dir: './dist',
          sourceMap: './dist/source-map.js',
          mainConfigFile:'./main.js',
          modules: [
            {
              name:'main'
            },
            {
              name:'app',
              findNestedDependencies: true
            }
          ]
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n' 
          + '//# sourceMappingURL=./source-map.js\n'  
          + 'require("source-map-support").install();',
        sourceMap: './dist/source-map.js'
      },
      dist: {
        dest: 'dist/<%= pkg.name %>.js',
        src: ['*.js', 'routes/**/*.js', '!Gruntfile.js']
      //  src: ['<%= concat.dist.dest %>']
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, src: ['./locales/*'], dest: 'dist/'},
          {expand: true, src: ['./package.json'], dest: 'dist/'}
        ]
      }
    },
    'sftp-deploy': {
      build: {
        auth: {
          host: '211.254.97.80',
          port: 22,
          authKey: 'rootkey'
        },
        src: ['./dist'],
        dest: '/securegate/node_home/rclientacceptor',
        exclusions: ['./**/.DS_Store', './**/*.concat.js'],
        server_sep: '/'
      }
    },
    watch: {
        js:  { files: ['*.js', 'routes/**/*.js', '!Gruntfile.js'], tasks: [ 'uglify', 'sftp-deploy' ] },
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-sftp-deploy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  // Default task(s).
  grunt.registerTask('default', ['requirejs','copy', 'sftp-deploy']);

};