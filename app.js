
/**
 * Module dependencies.
 */

var cluster = require('cluster');
var os = require('os');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user.min');
var https = require('https');
var path = require('path');
var clientconfig = require('./routes/api/client_config.min');
var mcc_ip_ranges = require('./routes/api/static_ip_range.min');
var client_logout = require('./routes/api/logout.min');
var fs = require('fs');
var app = express();
var i18n = require('i18n');

// CPU Count 를 출력합니다.
var cpuCount = os.cpus().length;

if (cluster.isMaster) {
    // 마스터 프로세스일 때
    for (var i = 0; i < cpuCount; i++) {
        var worker = cluster.fork();
        worker.send('hi worker' + worker.id);
    }

    cluster.on('exit', function(worker) {
        console.log('worker' + worker.pid + ' died');
        cluster.fork();
    })
} else {

    // all environments
    app.set('port', process.env.PORT || 443);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');


    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(i18n.init);
    app.use(app.router);
    //app.use(express.static(path.join(__dirname, 'public')));

    // development only
    if ('development' == app.get('env')) {
      app.use(express.errorHandler());
    }

    i18n.configure({
        locales: ['en', 'ko', 'ja'],
        defaultLocale:'ko',
        directory: __dirname + '/locales'
    });

    app.get('/', routes.index);
    // 윈도우 클라이언트에서 접속
    app.get('/api/client/config', clientconfig.get_config);
    // mcc 에서 static ip range 제공, 제공된 ip range 제외하고 동적 할당, 변경될시 client 접속 끊지 않음
    app.get('/api/update_mcc_ip_range', mcc_ip_ranges.update_mcc_ip_ranges);
    app.get('/api/client/logout', client_logout.client_logout);
    app.get('/users', user.list);

    var options = {
        key: fs.readFileSync(__dirname + '/certs/privatekey.pem'),
        cert: fs.readFileSync(__dirname + '/certs/certificate.pem')
    };

    https.createServer(options, app).listen(app.get('port'), function(){
      console.log('Express server listening on port ' + app.get('port'));
    });
}