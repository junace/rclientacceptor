/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 9. 10.
 * Time: 오후 2:04
 * To change this template use File | Settings | File Templates.
 */
var mysql = require('mysql');
var q = require('q');
var _ = require('lodash');
var util = require('util');

var legacyConnection = {
  connectionMap: {}
};

var localOptions = {
  host : 'localhost',
  user : 'root' ,
  password : 'hsmsecure',
  database : 'ssl_vpn_full'
};

var localConnection = mysql.createConnection(localOptions);
/*
localConnection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + localConnection.threadId);
});
*/
function reloadConnection() {
  var oldConnectionMap = legacyConnection.connectionMap;
 // console.log('reloadConnection');
  //console.log(util.inspect(oldConnectionMap));
 // console.log(util.inspect(localConnection));

  var newConnectionMap = {};
  var query_string = "select SSL_IP,SSL_ID from SSLINFO where SSL_SECTOR='1'";
  /*
  localConnection.query(query_string, function(err, rows) {
    console.log('query result');
    console.log(util.inspect(rows));
  });
  */
  var deferred = q.defer();
  localConnection.query(query_string, function(err, results) {
    if(err) {
      deferred.reject(err.stack())
    }
   // console.log('nfcall result');
  //  console.log(results);
    _.forEach(results, function (item) {
   //   console.log('forEach');
      if (_.has(oldConnectionMap, item.SSL_IP)) {
      //  console.log('contained');
        newConnectionMap[item.SSL_IP] = oldConnectionMap[item.SSL_IP];
        delete oldConnectionMap[item.SSL_IP];
        return;
      }

      if (item.SSL_ID && item.SSL_ID == 1) {
        newConnectionMap['local'] = localConnection;
       // console.log(util.inspect(newConnectionMap));
        //		console.log(legacyConnection.connectionArray);
        return;
      }
      var options = {
        host: item.SSL_IP,
        user: 'root',
        password: 'hsmsecure',
        database: 'ssl_vpn_full'
      };
      var c = mysql.createConnection(options);
      if (c) {
        newConnectionMap[item.SSL_IP] = c;
      }
    });
   // console.log('new connection map');
   // console.log(util.inspect(newConnectionMap));
   // console.log('old connection map');
  //  console.log(util.inspect(newConnectionMap));
    _.forEach(oldConnectionMap, function(v, k) {
      if(k === 'local') {
        return;
      }
  //    console.log('before end');
      v.end();
    });
    legacyConnection.connectionMap = newConnectionMap;
    deferred.resolve();
  });
  return deferred.promise;
}

reloadConnection();

legacyConnection.query = function(sql, values, cb) {
  var self = this;
  reloadConnection()
    .then(function() {
      if(sql.search(/select/i) == -1) {
        //	console.log('sql => ' + sql);
        //	console.log('not select sql');
        _.forEach(self.connectionMap, function(ele, key) {
          //		console.log('idx => ' + idx);
          if(key != 'local') {
        //    console.log('change callback');
            cb = function(err) {
           //   console.log('mysql backup server operation completed');
              if(err != null)
              {
                console.log('error occurred in operation in mysql sync operation' + err.message);
              }
            };
          }
          ele.query(sql, values, cb);
        });
      }
      else
      {
        //	console.log(this.connectionArray);
        self.connectionMap['local'].query(sql, values, cb);
      }
    })
    .fail(function(error) {
      console.log(error.stack);
    })
    .done();
};

exports.getConnection = function() {
	return legacyConnection;
};

exports.legacy_log = function(loginId, clientIp, level, msg) {
	var query_string = 'insert into INSPECTLOG(I_SERVICE,I_USERID,I_USERIP,I_LEVEL,I_CREATE_DATE,I_ACTION) values("cs client login",?,?,?,NOW(),?)';
	localConnection.query(query_string, [loginId, clientIp, level, msg], function(err, results) {
	});
};

exports.login_out_log = function(loginId, clientIp, natIp, logIn) {
//	console.log(clientIp);
	var query_string = 'insert into LOGINOUTLOG(L_ID,L_CLIENTIP,L_NATIP,L_FLAG,L_DATE,L_EQUIP) values(?,?,?,?,NOW(),?)';
	localConnection.query(query_string, [loginId, clientIp, natIp, logIn==true?1:2, 'pc'], function(err, results) {
		if(err) {
			console.log(err.message);
		}
	});
};