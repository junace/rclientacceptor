/**
 * Created with JetBrains WebStorm.
 * User: jun
 * Date: 13. 9. 9.
 * Time: 오후 3:26
 * To change this template use File | Settings | File Templates.
 */
 define( function() {
	return {
		ipcompare : function(a, b)
			{
				var fromArr = a.split(".");
				var toArr = b.split(".");
				for(i=0;i<4;i++)
				{
					if(fromArr[i]>toArr[i])
						return false;
				}
				return true;
			}
		}
 });
