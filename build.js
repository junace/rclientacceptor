requirejs.config({
    paths: {
        'app': './app.js',
        'mysql_legacy':  './mysql_legacy.js',
        'utils': './utils.js',
        'client_config': './routes/api/client_config.js',
        'logout': './routes/api/logout.js',
        'mcc_auth': './routes/api/mcc_auth.js',
        'static_ip_range': './routes/api/static_ip_range.js'
    },
    shim: {
    },
     nodeRequire: 'require'
});